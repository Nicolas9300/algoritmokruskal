package modelo;

import java.util.*;

public class Grafo {

	// Representamos el grafo por su matriz de adyacencia con A
	private Camino[][] A;
	private ArrayList<Camino> listaAristas;

	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo(int vertices) {
		A = new Camino[vertices][vertices];
		listaAristas = new ArrayList<Camino>();
	}

	// Agregado de aristas
	public void agregarArista(int i, int j, float peso) {

		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		Camino c = new Camino(i, j, peso, true);
		listaAristas.add(c);

		A[i][j] = c;
		A[j][i] = c;
	}

	// Eliminar aristas
	public void eliminarArista(int i, int j) {

		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		A[i][j].setConectado(false);
		A[j][i].setConectado(false);
	}

	// Informa si existe la arista mandada
	public boolean existeArista(int i, int j) {

		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		if (A[i][j] == null)
			return false;
		return A[i][j].getArista();
	}

	// Cantidad de Vertices
	public int tamanio() {
		return A.length;
	}

	// Vecinos de un vertice
	public Set<Integer> vecinos(int i) {

		verificarVertice(i);

		Set<Integer> ret = new HashSet<Integer>();

		for (int j = 0; j < this.tamanio(); j++)
			if (i != j) {

				if (this.existeArista(i, j))
					ret.add(j);

				/*
				 * if(A[i][j] == true) ret.add(j);
				 */
			}

		return ret;
	}

	// Verificar Vertice Valido
	private void verificarVertice(int i) {
		if (i < 0)
			throw new IllegalArgumentException("Vertice no puede ser negativo" + i);
		if (i >= A.length)
			throw new IllegalArgumentException("Los vertices son de 0 a 4" + i);
	}

	// Verificar que no sean iguales
	private void verificarDistintos(int i, int j) {
		if (i == j)
			throw new IllegalArgumentException("No se permiten vertices repetidos" + i + "-" + j);
	}

	public float getPeso(int i, int a) {
		return A[i][a].getPeso();
	}

	public boolean getEstaConectado(int i, int a) {
		if (A[i][a] == null) {
			return false;
		}
		return A[i][a].getArista();
	}

	public ArrayList<Camino> listaDeAristas() {
		return listaAristas;
	}
}
