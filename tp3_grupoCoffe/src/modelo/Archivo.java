package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;



public class Archivo {
	
	public static String leer(String archivo) {

		String texto = "";

		try {

			BufferedReader br = new BufferedReader(new FileReader(archivo));

			String linea;
			while ((linea = br.readLine()) != null) {
				texto += linea;
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

		

		return texto;

	}
	
	public static void guardar(String texto, String archivoDestino) {

		 try {
	            String ruta = archivoDestino;
	            String contenido = texto;
	            File file = new File(ruta);
	            // Si el archivo no existe es creado
	            if (!file.exists()) {
	                file.createNewFile();
	            }
	            // Se le pasa el archivo y se activa el flag para poder appendear
	            FileWriter fw = new FileWriter(file,true);
	            if(contenido != "\n") {
	            	fw.append(contenido).append(";");
	            }
	            else {fw.append(contenido);
	            }
	            fw.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	}
}
