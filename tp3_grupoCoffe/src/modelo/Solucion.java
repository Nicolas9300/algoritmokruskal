package modelo;

public class Solucion {

    double tiempoFinalBFS = 0, tiempoFinalUnionFind= 0;
	
	public void generarGrafo(String vertices, String aristas) {
		
		Grafo grafoAleatorio1;
		Kruskal k = new Kruskal();
		
		
		
		/*---------     Kruskal con BFS     ---------*/
		for(int i = 0; i < 10; i++) {
			grafoAleatorio1 = GeneradorGrafo.grafoRandom(Integer.parseInt(vertices), Integer.parseInt(aristas));
			long inicio = System.currentTimeMillis();
			k.recorrerConBFS(grafoAleatorio1);
			long fin = System.currentTimeMillis();
		    tiempoFinalBFS = tiempoFinalBFS + (fin - inicio);
		}
		tiempoFinalBFS = tiempoFinalBFS / 10;
		
		
		
		/*---------     Kruskal con UnionFind     ---------*/
		for(int j = 0; j < 10; j++) {
			grafoAleatorio1 = GeneradorGrafo.grafoRandom(Integer.parseInt(vertices), Integer.parseInt(aristas));
			long inicio2 = System.currentTimeMillis();
			k.recorrerConUnionFind(grafoAleatorio1);
			long fin2 = System.currentTimeMillis();
		    tiempoFinalUnionFind = tiempoFinalUnionFind + (fin2 - inicio2);
		}
		
		tiempoFinalUnionFind = tiempoFinalUnionFind / 10;
		
		
	}
	
	public double getTiempoBFS() {
		return tiempoFinalBFS;
	}
	
	public double getTiempoUnionFind() {
		return tiempoFinalUnionFind;
	}
	
}
