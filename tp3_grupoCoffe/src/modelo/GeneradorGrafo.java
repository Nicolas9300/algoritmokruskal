package modelo;

import java.util.Random;

public class GeneradorGrafo {

	public static Grafo grafoRandom(int vertices, int aristas) {

		Random r = new Random();

		int v = vertices;
		int a = aristas;

		Grafo gRandom = new Grafo(vertices);
		int cont = 0;
		while (cont < a) {

			int origenR = r.nextInt(v);
			int destinoR = r.nextInt(v);
			float pesoR = r.nextFloat();
			while (origenR == destinoR) {
				destinoR = r.nextInt(v);
			}
			if (!gRandom.existeArista(origenR, destinoR) && origenR != destinoR) {
				gRandom.agregarArista(origenR, destinoR, pesoR);
				cont++;
			}
		}
		return gRandom;
	}
}
