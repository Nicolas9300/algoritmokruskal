package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Test;

import modelo.Camino;
import modelo.Grafo;
import modelo.Kruskal;

public class KruskalTest {

	@Test
	public void caminoIgualConBFS() {
		Grafo g1 = inicializarGrafo();
		Kruskal k1 = new Kruskal();
		Grafo g2 = inicializarGrafo2();
		Kruskal k2 = new Kruskal();
		Grafo g3 = inicializarGrafo3();
		Kruskal k3 = new Kruskal();
		ArrayList<Camino> AGM = k1.recorrerConBFS(g1);

		ArrayList<Camino> esperado = new ArrayList<Camino>();

		esperado.add(new Camino(0, 1, 3, true));
		esperado.add(new Camino(0, 2, 1, true));
		esperado.add(new Camino(0, 3, 6, true));
		esperado.add(new Camino(3, 4, 7, true));

		
		assertTrue(Asserts.arreglosIguales(AGM, esperado));
		/*--------------------------------------------*/
		
		ArrayList<Camino> AGM2 = k2.recorrerConBFS(g2);
		
		ArrayList<Camino> esperado2 = new ArrayList<Camino>();
		
		esperado2.add(new Camino(0, 1, 4, true));
		esperado2.add(new Camino(3, 0, 8, true));
		esperado2.add(new Camino(2, 4, 3, true));
		esperado2.add(new Camino(3, 7, 1, true));
		esperado2.add(new Camino(7, 6, 3, true));
		esperado2.add(new Camino(7, 2, 5, true));
		esperado2.add(new Camino(4, 5, 6, true));
		esperado2.add(new Camino(2, 3, 6, true));
		
		assertTrue(Asserts.arreglosIguales(AGM2, esperado2));
		
		/*--------------------------------------------*/
		
		ArrayList<Camino> AGM3 = k3.recorrerConBFS(g3);
		
		ArrayList<Camino> esperado3 = new ArrayList<Camino>();
		
		esperado3.add(new Camino(0, 2, 3, true));
		esperado3.add(new Camino(2, 1, 5, true));
		esperado3.add(new Camino(1, 5, 2, true));
		esperado3.add(new Camino(1, 4, 3, true));
		esperado3.add(new Camino(5, 3, 4, true));
		esperado3.add(new Camino(5, 6, 6, true));
		esperado3.add(new Camino(5, 7, 9, true));
		esperado3.add(new Camino(2, 8, 8, true));
		
		
		assertTrue(Asserts.arreglosIguales(AGM3, esperado3));
	}
	
	@Test
	public void caminoIgualConUnionFind() {
		Grafo g1 = inicializarGrafo();
		Kruskal k1 = new Kruskal();
		Grafo g2 = inicializarGrafo2();
		Kruskal k2 = new Kruskal();
		Grafo g3 = inicializarGrafo3();
		Kruskal k3 = new Kruskal();

		ArrayList<Camino> ABUnionFind = k1.recorrerConUnionFind(g1);
		
		ArrayList<Camino> esperado = new ArrayList<Camino>();
		
		esperado.add(new Camino(0, 1, 3, true));
		esperado.add(new Camino(0, 2, 1, true));
		esperado.add(new Camino(0, 3, 6, true));
		esperado.add(new Camino(3, 4, 7, true));
		
		assertTrue(Asserts.arreglosIguales(ABUnionFind, esperado));
		
		
		/*--------------------------------------------*/
		
		ArrayList<Camino> ABUnionFind2 = k2.recorrerConUnionFind(g2);
		ArrayList<Camino> esperado2 = new ArrayList<Camino>();
		
		esperado2.add(new Camino(0, 1, 4, true));
		esperado2.add(new Camino(0, 3, 8, true));
		esperado2.add(new Camino(4, 2, 3, true));
		esperado2.add(new Camino(3, 7, 1, true));
		esperado2.add(new Camino(7, 6, 3, true));
		esperado2.add(new Camino(4, 6, 4, true));
		esperado2.add(new Camino(4, 5, 6, true));
		esperado2.add(new Camino(5, 8, 9, true));
		
		assertTrue(Asserts.arreglosIguales(ABUnionFind2, esperado2));
		
		
		
		/*--------------------------------------------*/
		
		ArrayList<Camino> ABUnionFind3 = k3.recorrerConUnionFind(g3);
		ArrayList<Camino> esperado3 = new ArrayList<Camino>();
		
		esperado3.add(new Camino(2, 0, 3, true));
		esperado3.add(new Camino(1, 2, 5, true));
		esperado3.add(new Camino(1, 5, 2, true));
		esperado3.add(new Camino(4, 1, 3, true));
		esperado3.add(new Camino(3, 5, 4, true));
		esperado3.add(new Camino(5, 6, 6, true));
		esperado3.add(new Camino(5, 7, 9, true));
		esperado3.add(new Camino(2, 8, 8, true));
		
		assertTrue(Asserts.arreglosIguales(ABUnionFind3, esperado3));
		
		
	}

	
	
	private Grafo inicializarGrafo() {

		Grafo g1 = new Grafo(5);
		g1.agregarArista(0, 1, 3);
		g1.agregarArista(0, 2, 1);
		g1.agregarArista(0, 3, 6);
		g1.agregarArista(2, 1, 4);
		g1.agregarArista(1, 3, 8);
		g1.agregarArista(3, 4, 7);
		g1.agregarArista(2, 4, 9);
		
		return g1;
	}
	
	private Grafo inicializarGrafo2() {
		Grafo  g2 = new Grafo(9);
		g2.agregarArista(0, 1, 4);
		g2.agregarArista(0, 3, 8);
		g2.agregarArista(1, 4, 8);
		g2.agregarArista(1, 3, 12);
		g2.agregarArista(3, 2, 6);
		g2.agregarArista(4, 2, 3);
		g2.agregarArista(0, 1, 4);
		g2.agregarArista(2, 7, 5);
		g2.agregarArista(3, 7, 1);
		g2.agregarArista(7, 6, 3);
		g2.agregarArista(4, 6, 4);
		g2.agregarArista(4, 5, 6);
		g2.agregarArista(5, 6, 13);
		g2.agregarArista(5, 8, 9);
		g2.agregarArista(0, 1, 4);
		g2.agregarArista(6, 8, 10);
		
		return g2;
	}
	
	private Grafo inicializarGrafo3() {
		Grafo g3 = new Grafo(9);
		
		g3.agregarArista(4, 1, 3);
		g3.agregarArista(4, 5, 4);
		g3.agregarArista(1, 5, 2);
		g3.agregarArista(1, 3, 9);
		g3.agregarArista(3, 5, 4);
		g3.agregarArista(1, 2, 5);
		g3.agregarArista(3, 6, 7);
		g3.agregarArista(5, 6, 6);
		g3.agregarArista(5, 7, 9);
		g3.agregarArista(7, 8, 10);
		g3.agregarArista(2, 6, 11);
		g3.agregarArista(2, 8, 8);
		g3.agregarArista(2, 0, 3);
		g3.agregarArista(0, 8, 9);
		
		return g3;
	}
}
