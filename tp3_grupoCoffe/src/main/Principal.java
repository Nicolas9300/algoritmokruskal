package main;

import controlador.Controlador;
import modelo.Kruskal;
import vista.Interfaz;

public class Principal {

	public static void main(String[] args) {
		Kruskal k = new Kruskal();
		Interfaz i = new Interfaz();
		Controlador c = new Controlador(k,i);
		c.iniciar();
		
		
	}

}
