package main;

import java.util.Scanner;

import grafos.Grafo;
import grafos.Kruskal;

public class Main {

	public static void main(String[] args) {
		Grafo grafoAleatorio;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese cantidad de vertices: ");
		int vertices = scan.nextInt();
		
		System.out.println("Ingrese cantidad de aristas: ");
		int aristas = scan.nextInt();
		
		grafoAleatorio = GeneradorGrafo.grafoRandom(vertices, aristas);
		
		Kruskal k = new Kruskal(grafoAleatorio);
		
		/*---------     Kruskal con BFS     ---------*/
		long inicio= System.currentTimeMillis();
		k.recorrerConBFS();
		long fin = System.currentTimeMillis();
		long tiempoFinalBFS = fin - inicio;
		System.out.println("El tiempo de kruskal con BFS es: "+ tiempoFinalBFS);
		
		
		/*---------     Kruskal con UnionFind     ---------*/
		long inicio2 = System.currentTimeMillis();
		k.recorrerConUnionFind();
		long fin2 = System.currentTimeMillis();
		long tiempoFinalUnionFind = fin2 - inicio2;
		System.out.println("El tiempo de kruskal con UnionFind es: "+ tiempoFinalUnionFind);
		
	}

}
