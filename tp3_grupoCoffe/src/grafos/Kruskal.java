package grafos;

import java.util.ArrayList;

import java.util.Collections;


public class Kruskal {
	
	private static Integer padres[];
	private Grafo g;
	
	public Kruskal(Grafo grafo) {
		g = grafo;
	}
	
	public  ArrayList<Camino> recorrerConBFS() {
		
		
		if(!BFS.esConexo(g, 0)) {
			throw new RuntimeException("El grafo no es conexo!");
		}
		ArrayList<Camino> caminos = new ArrayList<Camino>();
		Grafo grafo2 = new Grafo(g.tamanio());
		int contador = 1;

		while (contador <= g.tamanio() - 1) {
			float numeroMin = 1000;
			int x = 0;
			int y = 0;
			for (int i = 0; i < g.tamanio(); i++) {
				for (int a = 0; a < g.tamanio(); a++) {
					if (g.getEstaConectado(i, a)) {
						if (numeroMin > g.getPeso(i, a)) {
							if (!estaDestino(caminos, a)) {
								numeroMin = g.getPeso(i, a);
								x = i;
								y = a;
							}
						}
					}
				}
			}
			if (!BFS.esConexo(grafo2, x) && !BFS.alcanzables(grafo2, x).contains(y)) {
				caminos.add(new Camino(x, y, numeroMin, true));
			}
			g.eliminarArista(x, y);
			contador++;
		}
		
		
		return caminos;
		
	}

	public boolean estaDestino(ArrayList<Camino> caminos, int y) {

		for (int i = 0; i < caminos.size(); i++) {

			if (caminos.get(i).getDestino() == y)
				return true;

		}

		return false;

	}

	public ArrayList<Camino> recorrerConUnionFind() {
		ArrayList<Camino> arbolMinimoUnionFind = new ArrayList<Camino>();
		ArrayList<Camino> aristas = g.listaDeAristas();

		// Ordeno las aristas
		Collections.sort(aristas);

		/* Creo conjunto disjuntos */
		padres = new Integer[g.tamanio()];
		int size[] = new int[g.tamanio()];

		int cont = 1;
		for (int vertice = 0; vertice < g.tamanio(); vertice++) {
			padres[vertice] = vertice;
			size[vertice] = 1;
		}
		while (cont <= g.tamanio() - 1) {

			for (int i = 0; i < aristas.size(); i++) {
				int origen = aristas.get(i).origen;
				int destino = aristas.get(i).destino;

				if (!sameComponent(origen, destino, padres)) {
					arbolMinimoUnionFind.add(aristas.get(i));
					union(origen, destino, padres, size);
				}
			}

			cont++;
		}
		
		return arbolMinimoUnionFind;
	}

	public  int find(int i, Integer[] padres) {
		if (padres[i] == i) {
			return i;
		} else {
			/* path compression */
			/* que todos los nodos apunten a la raiz */
			padres[i] = find(padres[i], padres);
			return padres[i];
		}
	}

	public  void union(int u, int v, Integer[] padres, int[] size) {
		/*
		 * Busco los padres de los vertices u y v Uno el conjunto de vertices menor con
		 * el mas grande!
		 */

		u = find(u, padres);
		v = find(v, padres);
		if (size[u] > size[v]) {
			padres[v] = u;
			size[u] += size[v];
		} else {
			padres[u] = v;
			size[v] += size[u];
		}
	}

	// Metodo para saber si dos vertices estan en la misma componente conexa
	public  boolean sameComponent(int x, int y, Integer[] padres) {
		if (find(x, padres) == find(y, padres))
			return true;
		return false;
	}
}
