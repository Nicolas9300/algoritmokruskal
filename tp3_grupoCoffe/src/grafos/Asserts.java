package grafos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Set;

public class Asserts {
	
  public static void iguales(int[] esperado, Set<Integer> obtenido) {
		
		assertEquals(esperado.length, obtenido.size());
		
		for(int i= 0; i < esperado.length; i++)
			assertTrue(obtenido.contains(esperado[i]));
		
	}
  
  public static boolean arreglosIguales(ArrayList<Camino> listaGenerada,ArrayList<Camino> listaEsperada) {
	  
	  boolean esIgual = true;
	  for(int i = 0; i < listaEsperada.size(); i++) {	  
		  esIgual = esIgual && listaEsperada.contains(listaGenerada.get(i));	  
	  }
	  return esIgual;
	  
  }

}
