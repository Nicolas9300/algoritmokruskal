package grafos;

public class Camino implements Comparable<Camino> {

	public int origen, destino;
	public float peso;
	public boolean conectados;

	public Camino(int origen, int destino, float peso, boolean a) {
		this.origen = origen;
		this.destino = destino;
		this.peso = peso;
		conectados = a;
	}

	public void setConectado(boolean b) {
		conectados = b;
	}

	public boolean getArista() {
		return conectados;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(int p) {
		this.peso = p;
	}

	public int getDestino() {
		return destino;
	}
	
	public int getOrigen() {
		return origen;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (conectados ? 1231 : 1237);
		result = prime * result + destino;
		result = prime * result + origen;
		result = (int) (prime * result + peso);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Camino))
			return false;
		Camino other = (Camino) obj;
		if (conectados != other.conectados)
			return false;
		if (destino != other.destino)
			return false;
		if (origen != other.origen)
			return false;
		if (peso != other.peso)
			return false;
		return true;
	}

	@Override
	public int compareTo(Camino o) {
		return (int) (this.peso - o.peso);
	}

}
