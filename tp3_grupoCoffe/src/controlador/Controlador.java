package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import modelo.Kruskal;
import modelo.Archivo;
import vista.Interfaz;
import modelo.Solucion;

public class Controlador implements ActionListener {
	private JFreeChart grafico;
	@SuppressWarnings("unused")
	private Kruskal kruskal;
	private Interfaz interfaz;
	private String aristas;
	private String vert;
	private double tiempoBFS;
	private double tiempoUnionFind;

	public Controlador(Kruskal k, Interfaz i) {
		kruskal = k;
		interfaz = i;
		interfaz.btnGenerar.addActionListener(this);
		interfaz.btnMostrarGrafico.addActionListener(this);
	}

	public void iniciar() {
		interfaz.setTitle("Grupo Coffee: Algoritmo Kruskal");
		interfaz.frame.setLocationRelativeTo(null);
		interfaz.frame.setResizable(false);
		interfaz.frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == interfaz.btnGenerar) {
			ejecutar();
		}
		if(e.getSource() == interfaz.btnMostrarGrafico) {
			grafico = crearGraficoLineal("Kruskal con UnionFind VS Kruskal con BFS","Tama�o", "Tiempo");
			ChartPanel panel = new ChartPanel(grafico);
			JFrame ventana = new JFrame("Grafico lineal de Kruskal");
			ventana.setBounds(100, 100, 550, 400);
			ventana.setLocationRelativeTo(null);
			ventana.setResizable(false);
			ventana.getContentPane().add(panel);
			ventana.setVisible(true);
			
		}

	}

	private void ejecutar() {
		Solucion solucion = new Solucion();
		aristas = interfaz.textFieldAristas.getText();
		vert = interfaz.textFieldVertices.getText();
		Archivo.guardar(vert,"src/datos/datos.txt");
		solucion.generarGrafo(vert, aristas);
		tiempoBFS = solucion.getTiempoBFS();
		tiempoUnionFind = solucion.getTiempoUnionFind();
		Archivo.guardar(String.valueOf(tiempoBFS), "src/datos/datos.txt");
		Archivo.guardar(String.valueOf(tiempoUnionFind), "src/datos/datos.txt");
		Archivo.guardar("\n", "src/datos/datos.txt");
		interfaz.lblBFS2.setText(String.valueOf(tiempoBFS));
		interfaz.lblUnionFind2.setText(String.valueOf(tiempoUnionFind));
	}
	
	private JFreeChart crearGraficoLineal(String titulo, String primerVariable, String segundaVariable) {
		DefaultCategoryDataset datos = crearDatos();
		
		return ChartFactory.createLineChart(titulo, primerVariable, segundaVariable, datos, PlotOrientation.VERTICAL, true, false, false);
	}

	private DefaultCategoryDataset crearDatos() {
		DefaultCategoryDataset aux = new DefaultCategoryDataset();
		if(vert == null) {
			throw new IllegalArgumentException("No hay datos para graficar");
		}
		
		
		String datosExternos = Archivo.leer("src/datos/datos.txt");
		String[] datosParseados = datosExternos.split(";");
		
		String vertice = "";
		int flagValores = 0;
		double tiempoBFS = 0;
		double tiempoUnionFind = 0;
		for(int i = 0; i < datosParseados.length; i++) {
			if(i % 3 == 0) {
				vertice = datosParseados[i];
				flagValores = 0;
			}else {
				if(i < datosParseados.length-1) {
					if(flagValores == 1) {
						tiempoBFS = Double.parseDouble(datosParseados[i]);
						aux.addValue(tiempoBFS, "BFS", vertice);
					}else {
					tiempoUnionFind = Double.parseDouble(datosParseados[i]);
					aux.addValue(tiempoUnionFind, "UnionFind", vertice);
					}
				}
				else {
					tiempoUnionFind = Double.parseDouble(datosParseados[datosParseados.length-1]);
					aux.addValue(tiempoUnionFind, "UnionFind", vertice);
				}
			}
			flagValores++;
		}
		
		return aux;
	}
}
