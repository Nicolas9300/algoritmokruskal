package vista;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import modelo.Solucion;
public class Interfaz extends JFrame{

	/**
	 * 
	 */
	public JFrame frame;
	public JButton btnGenerar;
	public JButton btnMostrarGrafico;
	public JLabel lblBFS,lblUnionFind,lblBFS2,lblUnionFind2;
	public JTextField textFieldVertices;
	public JTextField textFieldAristas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e){
			
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 735, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JLabel lblCantVert = new JLabel("Cantidad de Vertices:");
		lblCantVert.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblCantVert.setBounds(46, 161, 203, 23);
		frame.getContentPane().add(lblCantVert);
		
		JLabel lblCantAristas = new JLabel("Cantidad de Aristas:");
		lblCantAristas.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblCantAristas.setBounds(46, 233, 203, 36);
		frame.getContentPane().add(lblCantAristas);
		
		JLabel lblTitulo = new JLabel("Algoritmo Kruskal");
		lblTitulo.setFont(new Font("Tekton Pro", Font.BOLD, 30));
		lblTitulo.setBounds(223, 34, 275, 49);
		frame.getContentPane().add(lblTitulo);
		
		textFieldVertices = new JTextField();
		textFieldVertices.setBounds(218, 164, 86, 23);
		frame.getContentPane().add(textFieldVertices);
		textFieldVertices.setColumns(10);
		
		textFieldAristas = new JTextField();
		textFieldAristas.setBounds(218, 242, 86, 25);
		frame.getContentPane().add(textFieldAristas);
		textFieldAristas.setColumns(10);
		
		btnGenerar = new JButton("Generar");
		btnGenerar.setBackground(Color.GREEN);
		btnGenerar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGenerar.setBounds(46, 315, 130, 49);
		frame.getContentPane().add(btnGenerar);
		
		btnMostrarGrafico = new JButton("Mostrar grafico");
		btnMostrarGrafico.setBackground(Color.GREEN);
		btnMostrarGrafico.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnMostrarGrafico.setBounds(438, 315, 159, 49);
		frame.getContentPane().add(btnMostrarGrafico);
		
	    lblBFS = new JLabel("BFS");
		lblBFS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBFS.setBounds(375, 160, 54, 25);
		frame.getContentPane().add(lblBFS);
		
	    lblUnionFind = new JLabel("UnionFInd");
		lblUnionFind.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblUnionFind.setBounds(375, 232, 106, 39);
		frame.getContentPane().add(lblUnionFind);
		
	    lblBFS2 = new JLabel("");
		lblBFS2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBFS2.setBounds(470, 162, 172, 22);
		frame.getContentPane().add(lblBFS2);
		
	    lblUnionFind2 = new JLabel("");
		lblUnionFind2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblUnionFind2.setBounds(477, 242, 165, 25);
		frame.getContentPane().add(lblUnionFind2);
		
		
	}
}
